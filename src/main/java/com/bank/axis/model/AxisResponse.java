package com.bank.axis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AxisResponse {
    private HttpStatus responseStatus;
    private String responseBody;
    private long responseTime;
}
