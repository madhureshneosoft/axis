package com.bank.axis.model;

import lombok.Data;

@Data
public class TransactionRequest {

    private CardDetails cardDetails;
    private int amount;
}
