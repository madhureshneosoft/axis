package com.bank.axis.model;

import lombok.Data;

@Data
public class CardDetails {
    private String cardNumber;
    private String cvv;
    private String expiryDate;
    private String pin;
}
