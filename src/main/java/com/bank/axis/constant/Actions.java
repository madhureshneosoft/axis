package com.bank.axis.constant;

import java.util.Arrays;
import java.util.List;

public class Actions {
    public static final String WITHDRAW = "withdraw";
    public static final String DEPOSIT = "deposit";
    public static final String CHECK_BALANCE = "check balance";

    public static final String VALID_CARD = "Valid Card Details";
    public static final String INVALID_CARD = "Invalid Card Details";
    public static final String INVALID_ACTION = "Action is not valid";
    public static final String CARD_EXPIRED = "Your card has been expired";

    public static List<String> getAllActions(){
        return Arrays.asList(WITHDRAW,DEPOSIT,CHECK_BALANCE);
    }
}
