package com.bank.axis.exception;

public class AtmException extends RuntimeException {

    public AtmException(String exception){
        super(exception);
    }
}
