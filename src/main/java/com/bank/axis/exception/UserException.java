package com.bank.axis.exception;

public class UserException extends RuntimeException {
    public UserException(String exception) {
        super(exception);
    }
}
