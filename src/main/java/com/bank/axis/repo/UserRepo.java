package com.bank.axis.repo;

import com.bank.axis.model.CardDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepo {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public Long isAuthenticate(CardDetails cardDetails){
        return jdbcTemplate.query("SELECT * from card_details_tbl where cardNo=? and expiryDate=? and cvv=? and pin=?", preparedStatement -> {
            preparedStatement.setString(1, cardDetails.getCardNumber());
            preparedStatement.setString(2, cardDetails.getExpiryDate());
            preparedStatement.setString(3, cardDetails.getCvv());
            preparedStatement.setString(4, cardDetails.getPin());
        },
                resultSet -> {
                    if (resultSet.next()) {
                        return resultSet.getLong(1);
                    }
                    return null;
                }
        );
    }

    public void withdrawMoney(int balance, long userId){
        jdbcTemplate.update("UPDATE card_details_tbl set balance='"+balance+"' WHERE `id`='"+userId+"'");
    }

    public void addMoneyToAccount(int balance, long userId){
        jdbcTemplate.update("UPDATE card_details_tbl set balance='"+balance+"' WHERE `id`='"+userId+"'");
    }

    public String checkBalance(long userId){
        return jdbcTemplate.queryForObject("SELECT balance from card_details_tbl WHERE `id`='"+userId+"'",String.class);
    }

}
