package com.bank.axis.service;

import com.bank.axis.model.CardDetails;

public interface UserService {
    Long isAuthenticated(CardDetails cardDetails);
    String checkBalance(long cardId);
    String withdraw(int amount, long cardId);
    String deposit(int amount, long cardId);
}
