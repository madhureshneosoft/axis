package com.bank.axis.service;

import com.bank.axis.exception.UserException;
import com.bank.axis.model.CardDetails;
import com.bank.axis.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepo userRepo;

    @Override
    public Long isAuthenticated(CardDetails cardDetails) {
        return userRepo.isAuthenticate(cardDetails);
    }

    @Override
    public String checkBalance(long cardId) {
        return userRepo.checkBalance(cardId);
    }

    @Override
    public String withdraw(int amount, long cardId) {
        int balance = Integer.parseInt(checkBalance(cardId));
        if(amount<=balance){
            userRepo.withdrawMoney(balance-amount,cardId);
            return "Successful";
        }
        throw new UserException("Not enough balance");
    }

    @Override
    public String deposit(int amount, long cardId) {
        int balance = Integer.parseInt(checkBalance(cardId));
        userRepo.addMoneyToAccount(balance+amount,cardId);
        return "Successful";
    }

}
