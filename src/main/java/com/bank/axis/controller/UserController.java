package com.bank.axis.controller;

import com.bank.axis.exception.AtmException;
import com.bank.axis.exception.UserException;
import com.bank.axis.model.AxisResponse;
import com.bank.axis.model.CardDetails;
import com.bank.axis.model.TransactionRequest;
import com.bank.axis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.bank.axis.constant.Actions.INVALID_CARD;
import static com.bank.axis.constant.Actions.VALID_CARD;

@RestController
@RequestMapping("/axis")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping("/")
    private String healthCheck() {
        return "Hey! i m healthy";
    }

    @PostMapping("/validate")
    private ResponseEntity<AxisResponse> validate(@RequestBody CardDetails cardDetails){
        long start = System.currentTimeMillis();
        long end;
        try {
            Long cardId = userService.isAuthenticated(cardDetails);
            if(cardId!=null){
                end = System.currentTimeMillis()-start;
                return HelperMethods.responseBuilder(VALID_CARD,end);
            } else {
                throw new AtmException(INVALID_CARD);
            }
        } catch (AtmException e){
            end = System.currentTimeMillis()-start;
            return HelperMethods.exceptionBuilder(e.getMessage(),end);
        }
    }

    @PostMapping("/checkBalance")
    private ResponseEntity<AxisResponse> checkBalance(@RequestBody CardDetails cardDetails) {
        long start = System.currentTimeMillis();
        long end;
        try {
            Long cardId = userService.isAuthenticated(cardDetails);
            if(cardId!=null){
                end = System.currentTimeMillis()-start;
                return HelperMethods.responseBuilder(userService.checkBalance(cardId),end);
            } else {
                throw new UserException(INVALID_CARD);
            }
        } catch (UserException e){
            end = System.currentTimeMillis()-start;
            return HelperMethods.exceptionBuilder(e.getMessage(),end);
        }
    }

    @PostMapping("/withdraw")
    private ResponseEntity<AxisResponse> withdraw(@RequestBody TransactionRequest transactionRequest) {
        long start = System.currentTimeMillis();
        long end;
        try {
            Long cardId = userService.isAuthenticated(transactionRequest.getCardDetails());
            if(cardId!=null){
                end = System.currentTimeMillis()-start;
                return HelperMethods.responseBuilder(userService.withdraw(transactionRequest.getAmount(),cardId),end);
            } else {
                throw new UserException(INVALID_CARD);
            }
        } catch (UserException e){
            end = System.currentTimeMillis()-start;
            return HelperMethods.exceptionBuilder(e.getMessage(),end);
        }
    }

    @PostMapping("/deposit")
    private ResponseEntity<AxisResponse> deposit(@RequestBody TransactionRequest transactionRequest) {
        long start = System.currentTimeMillis();
        long end;
        try {
            Long cardId = userService.isAuthenticated(transactionRequest.getCardDetails());
            if(cardId!=null){
                end = System.currentTimeMillis()-start;
                return HelperMethods.responseBuilder(userService.deposit(transactionRequest.getAmount(),cardId),end);
            } else {
                throw new UserException(INVALID_CARD);
            }
        } catch (UserException e){
            end = System.currentTimeMillis()-start;
            return HelperMethods.exceptionBuilder(e.getMessage(),end);
        }
    }

    /*@PostMapping("/transaction")
    private ResponseEntity<String> userAction(@RequestBody UserRequest userRequest) {
        try {
            Long cardId = userService.isAuthenticated(userRequest.getCardDetails());
            if(cardId!=null){
                return HelperMethods.responseBuilder(userService.actionResolver(cardId,userRequest));
            } else {
                throw new UserException(INVALID_CARD);
            }
        } catch (UserException e){
            return HelperMethods.exceptionBuilder(e.getMessage());
        }
    }*/
}