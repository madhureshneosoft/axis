package com.bank.axis.controller;

import com.bank.axis.model.AxisResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class HelperMethods {

    public static ResponseEntity<AxisResponse> exceptionBuilder(String exception, long time){
        return new ResponseEntity<>(new AxisResponse(HttpStatus.BAD_REQUEST, exception, time), HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity<AxisResponse> responseBuilder(String response, long time){
        return new ResponseEntity<>(new AxisResponse(HttpStatus.OK, response, time), HttpStatus.OK);
    }
}
